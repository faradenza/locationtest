package ru.startandroid.locationtest;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoginActivity extends AppCompatActivity {
    //конфиги
    public static final String APP_PREFERENCES = "LocationProjectConfig";
    public static final String APP_PREFERENCES_ATHOR = "false";
    public static final String APP_PREFERENCES_LOGIN = "login";
    public static final String APP_PREFERENCES_PASS = "pass";
    private SharedPreferences mSettings;

    final String LOG_TAG = "myLogs";

    Button btnLogin;
    EditText edLogin;
    EditText edPass;
    ProgressBar probar;
    String config_login;
    String config_pass;
    String auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);//конфиги
        Log.d(LOG_TAG, "Login onStart");
        Logs("unauthorized", "login login");
        btnLogin = (Button) findViewById(R.id.btn_login);
        edLogin = (EditText) findViewById(R.id.ed_login);
        edPass = (EditText) findViewById(R.id.ed_pass);
        probar = (ProgressBar) findViewById(R.id.progressBar);
        probar.setVisibility(View.VISIBLE);
        Logs("unauthorized", "login login");
        btnLogin.setVisibility(View.INVISIBLE);
        edLogin.setVisibility(View.INVISIBLE);
        edPass.setVisibility(View.INVISIBLE);
        if(!mSettings.contains(APP_PREFERENCES_ATHOR)){
            Logs("unauthorized", "create app preferences and set on def");
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString(APP_PREFERENCES_ATHOR, "false");
            editor.putString(APP_PREFERENCES_LOGIN, "none");
            editor.putString(APP_PREFERENCES_PASS, "none");
            editor.apply();
        }
        CheckLogin();

        View.OnClickListener OnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //начало анимации
               Logs("unauthorized", "click on button login");
                probar.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.INVISIBLE);
                edLogin.setVisibility(View.INVISIBLE);
                edPass.setVisibility(View.INVISIBLE);
                new RequestToServer().execute();

            }
        };
        btnLogin.setOnClickListener(OnClick);

    }
    @Override
    public void onStop(){
        super.onStop();
       Logs("unauthorized", "login activity onstop");

        this.finish();
    }
    public class MyThread extends Thread {
        private String login;
        private String log;
        public MyThread(String login, String log){
            this.login = login;
            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("hh:mm:ss");
            this.log = (String.format(log + " " + formatForDateNow.format(dateNow))).replace(" ", "%20");
        }
        public void run() {
            HttpClient httpclient = new DefaultHttpClient();
            Log.d(LOG_TAG, "http://130.193.35.127/get_logs.php" + "?login=" + login + "&text=" + log);
            HttpGet httpget = new HttpGet("http://130.193.35.127/get_logs.php" + "?login=" + login + "&text=" + log);
            try {
                HttpResponse response = httpclient.execute(httpget);
            }
            catch (IOException e) {
                Log.d(LOG_TAG, e.toString());
            }
        }
    }

    public void Logs(String login, String log){
    MyThread t = new MyThread(login, log);
    t.start();
    }

    public void CheckLogin(){
        //проверка, есть ли у тебя данные логина в конфигах
        Log.d(LOG_TAG, "Login->CheckLochin()");
       Logs("unauthorized", "login activity checklogin func");
        if (mSettings.contains(APP_PREFERENCES_ATHOR)) {
            auth = mSettings.getString(APP_PREFERENCES_ATHOR, "false");
            if(auth.equals("false")){
                //нет данных
                Log.d(LOG_TAG, "You have not config athorication!");
               Logs("unauthorized", "have not conf files");
                probar.setVisibility(View.INVISIBLE);
                btnLogin.setVisibility(View.VISIBLE);
                edLogin.setVisibility(View.VISIBLE);
                edPass.setVisibility(View.VISIBLE);
            }
            else{
                //проверка правильности данных
                Log.d(LOG_TAG, "Check your login&password authenticity");
               Logs("unauthorized", "have conf files try check it");
                if(mSettings.contains(APP_PREFERENCES_LOGIN)){
                    config_login = mSettings.getString(APP_PREFERENCES_LOGIN, "none");
                    Log.d(LOG_TAG, "Your login is ");
                    Logs("unauthorized", "Login is "+config_login);
                }
                if(mSettings.contains(APP_PREFERENCES_PASS)){
                    config_pass = mSettings.getString(APP_PREFERENCES_PASS, "none");
                    Log.d(LOG_TAG, "Your pass is ");
                    Logs("unauthorized", "Login is "+config_pass);
                }
                //запрос на сервер чтобы узнать правильность данных
                new RequestToServer().execute();
            }

        }else {
            //конец анимации, показ экрана логина
            Log.d(LOG_TAG, "You have not config athorication");
            Logs("unauthorized", "have not conf files");
            probar.setVisibility(View.INVISIBLE);
            btnLogin.setVisibility(View.VISIBLE);
            edLogin.setVisibility(View.VISIBLE);
            edPass.setVisibility(View.VISIBLE);
        }

    }


    class RequestToServer extends AsyncTask<String, String, String> {
        //функция запроса
        String answerHTTP;
        String editLogin;
        String editPass;
        String server = "http://130.193.35.127/authorization.php";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(LOG_TAG, "Start of request");
           Logs("unauthorized", "start of request");
                editLogin = edLogin.getText().toString();
                editPass = edPass.getText().toString();
        }

        @Override
        protected String doInBackground(String... params) {
            Log.d(LOG_TAG, "in doInBackground");
          Logs("unauthorized", "in doinbackground");
            if(auth.equals("false")){
                //если ты не залогинен, то запрос по полям
                Log.d(LOG_TAG, "in false auth");
               Logs("unauthorized", "try to login with edittext");
                HttpClient httpclient = new DefaultHttpClient();
                Log.d(LOG_TAG, "url: "+server + "?login=" + editLogin + "&pass=" + editPass);
                HttpGet httpget = new HttpGet(server + "?login=" + editLogin + "&pass=" + editPass);

                try {
                    HttpResponse response = httpclient.execute(httpget);

                    if (response.getStatusLine().getStatusCode() == 200) {
                        HttpEntity entity = response.getEntity();
                        answerHTTP = EntityUtils.toString(entity);
                    }
                }
                catch (ClientProtocolException e) {
                    Log.d(LOG_TAG,"error "+e);
                }
                catch (IOException e) {
                    Log.d(LOG_TAG,"error "+e);
                }
            }else{
                //если залогинен то запрос по конфигам
                Log.d(LOG_TAG, "in true auth");
                Logs("unauthorized", "try to login with conf");
                HttpClient httpclient = new DefaultHttpClient();
                config_login = mSettings.getString(APP_PREFERENCES_LOGIN, "none");
                config_pass = mSettings.getString(APP_PREFERENCES_PASS, "none");
                Log.d(LOG_TAG, "url: "+server + "?login=" + config_login + "&pass=" + config_pass);
                HttpGet httpget = new HttpGet(server + "?login=" + config_login + "&pass=" + config_pass);

                try {
                    HttpResponse response = httpclient.execute(httpget);
                    if (response.getStatusLine().getStatusCode() == 200) {
                        HttpEntity entity = response.getEntity();
                        answerHTTP = EntityUtils.toString(entity);
                    }
                }
                catch (ClientProtocolException e) {
                    Log.d(LOG_TAG,"error "+e.toString());
                }
                catch (IOException e) {
                    Log.d(LOG_TAG,"error "+e.toString());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(LOG_TAG,"in  onPostExecute");
           // Logs("unauthorized", "login activity onpostexecute");
            Logs("unauthorized", "answer: "+answerHTTP);
            Log.d(LOG_TAG,"answer: "+answerHTTP);

            //если пришел положительный ответ - сохранение данных в конфиги и переход на основную страницу
            if(answerHTTP.equals("true")){
                if(auth.equals("false")) {
                    Log.d(LOG_TAG, "write config");
                   Logs("unauthorized", "whrite conf files");
                    SharedPreferences.Editor editor = mSettings.edit();
                    editor.putString(APP_PREFERENCES_ATHOR, "true");
                    Log.d(LOG_TAG, editLogin + " " + edPass);
                    editor.putString(APP_PREFERENCES_LOGIN, editLogin);
                    editor.putString(APP_PREFERENCES_PASS, editPass);
                    editor.apply();
                }
                Log.d(LOG_TAG,"start activity");
               Logs("unauthorized", "try start mainactivity");
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }else{
                //конец анимации отображение полей логина
                probar.setVisibility(View.INVISIBLE);
                btnLogin.setVisibility(View.VISIBLE);
                edLogin.setVisibility(View.VISIBLE);
                edPass.setVisibility(View.VISIBLE);
            }
        }
    }
}
