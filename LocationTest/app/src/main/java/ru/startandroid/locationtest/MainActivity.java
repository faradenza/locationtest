package ru.startandroid.locationtest;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.app.AlertDialog;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    final String LOG_TAG = "myLogs"; //Тег лог-сообщений
    String ACCOUNT_NAME;
    //объявление переменных для view элементов
    TextView tvStatus;
    Button btnStatus;
    Button btnLogOut;
    TextView tvLogin;
    //данные о конфигах
    public static final String APP_PREFERENCES = "LocationProjectConfig";
    public static final String APP_PREFERENCES_ATHOR = "false";
    public static final String APP_PREFERENCES_LOGIN = "login";
    public static final String APP_PREFERENCES_PASS = "pass";
    private SharedPreferences mSettings;

    boolean StatusProgram; //Переменная, true=сервис запущен, false=сервиса нет

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);//конфиги
        setContentView(R.layout.activity_main);
        Log.d(LOG_TAG, "MainActivity->Start()");

        //инициализация переменных для view элементов
        btnStatus = (Button) findViewById(R.id.btnStopStart);
        tvStatus = (TextView) findViewById(R.id.StatusStatus);
        btnLogOut = (Button) findViewById(R.id.btnLogOut);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvLogin.setText(mSettings.getString(APP_PREFERENCES_LOGIN, "error"));
        ACCOUNT_NAME = mSettings.getString(APP_PREFERENCES_LOGIN, "error");
        //назначение обработчика нажатия на кнопки
        btnStatus.setOnClickListener(this);
        btnLogOut.setOnClickListener(this);
        Logs(ACCOUNT_NAME, "main acrivity start func");
        //Изменение интерфейса в зависимости от существования сервиса
        if(isMyServiceRunning(MyService.class)) {
            Log.d(LOG_TAG, "Service is Running");
            Logs(ACCOUNT_NAME, "service is running");
            StatusProgram = true;
            btnStatus.setText("Стоп");
        }
        else {
            StatusProgram = false;
            Log.d(LOG_TAG, "Service is stoping");
            Logs(ACCOUNT_NAME, "service is stoping");
        }
    }

    //функция обработки нажатия на кнопку
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            //логаут
            case R.id.btnLogOut:
                SharedPreferences.Editor editor = mSettings.edit();
                editor.putString(APP_PREFERENCES_ATHOR, "false");
                editor.putString(APP_PREFERENCES_LOGIN, "none");
                editor.putString(APP_PREFERENCES_PASS, "none");
                editor.apply();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                //Нажатие на кнопку Старт/Стоп
            case R.id.btnStopStart:
                Log.d(LOG_TAG, "Click button Start/Stop");
                Logs(ACCOUNT_NAME, "click button start stop");
                if (StatusProgram) {
                    btnStatus.setText("Начать");
                    tvStatus.setText("Выключено");
                    stopService(new Intent(MainActivity.this, MyService.class));
                    Log.d(LOG_TAG, "Intent 'stop server' was send");
                    Logs(ACCOUNT_NAME, "intent stop server was send");
                    StatusProgram = false;
                } else {
                    btnStatus.setText("Стоп");
                    tvStatus.setText("Включено");
                    startService(new Intent(MainActivity.this, MyService.class));
                    Log.d(LOG_TAG, "Intent 'start server' was created");
                    Logs(ACCOUNT_NAME, "intent start server was created");
                    StatusProgram = true;
                    break;
                }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) { //Функция возвращающая запущен ли наш сервис
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(isMyServiceRunning(MyService.class)) tvStatus.setText("Включено");
        else tvStatus.setText("Выключено");
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }

    public class MyThread extends Thread {
        private String login;
        private String log;
        public MyThread(String login, String log){
            this.login = login;
            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("hh:mm:ss");
            this.log = (String.format(log + " " + formatForDateNow.format(dateNow))).replace(" ", "%20");
        }
        public void run() {
            HttpClient httpclient = new DefaultHttpClient();
            Log.d(LOG_TAG, "http://130.193.35.127/get_logs.php" + "?login=" + login + "&text=" + log);
            HttpGet httpget = new HttpGet("http://130.193.35.127/get_logs.php" + "?login=" + login + "&text=" + log);
            try {
                HttpResponse response = httpclient.execute(httpget);
            }
            catch (IOException e) {
                Log.d(LOG_TAG, e.toString());
            }
        }
    }

    public void Logs(String login, String log){
        MyThread t = new MyThread(login, log);
        t.start();
    }
}

