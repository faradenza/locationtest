package ru.startandroid.locationtest;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyService extends Service {
    //Переменные, хранящие последний запрос геоданных
    String last_lantitude;
    String last_longitude;
    String last_time;
    String last_date;
    String login;
    String ACCOUNT_NAME;

    //конфиги
    public static final String APP_PREFERENCES = "LocationProjectConfig";
    public static final String APP_PREFERENCES_ATHOR = "false";
    public static final String APP_PREFERENCES_LOGIN = "login";
    public static final String APP_PREFERENCES_PASS = "pass";
    private SharedPreferences mSettings;

    private LocationManager locationManager;
    //создание переменной для локатор манагера
    final String LOG_TAG = "myLogs";
    //Тег логов

    public void onCreate() {
        super.onCreate();
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);//конфиги
        ACCOUNT_NAME = mSettings.getString(APP_PREFERENCES_LOGIN, "error");
        Log.d(LOG_TAG, "MyService->Start()");
        Logs(ACCOUNT_NAME, "myservice start");
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        //Инициализация локатор манагера
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "MyService->onStartCommand()");
        Logs(ACCOUNT_NAME, "myservice onstartcommand");
        someTask();
        //функция для получения геолокации
        return Service.START_STICKY;
        //START_STICKY - сервис будет перезапускаться автоматически
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(LOG_TAG, "MyService->onTaskRemoved()");
        Logs(ACCOUNT_NAME, "myservice ontastremuved");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "MyService->Destroy()");
        Logs(ACCOUNT_NAME, "myservice destroy");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    void someTask() {
        //запрос на разрешение прав отдава гео
        String auth = mSettings.getString(APP_PREFERENCES_ATHOR, "false");
        //если у тебе нет права отдавать гео -> серис закрывается
        if(auth.equals("true")) {
            login = mSettings.getString(APP_PREFERENCES_LOGIN, "none");
            Log.d(LOG_TAG, "MyService->someTask()");
            Logs(ACCOUNT_NAME, "myservice sometask");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 5, 2, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 5, 2, locationListener);
        }
        else stopSelf();

    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(LOG_TAG, "MyService->onLicationChanged");
            Logs(ACCOUNT_NAME, "myservice onlicationchanged");
            ShowLocation(location);//если локация сменилась то запускается передача гео
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    private void ShowLocation(Location location){
        if(location == null) return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
//Если геолокация передана с помощью GPS
            Log.d(LOG_TAG, "Geo from GPS");
            Logs(ACCOUNT_NAME, "geo from gps");
        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
//Если геолокация передана с помощью Internet
            Log.d(LOG_TAG, "Geo from internet");
            Logs(ACCOUNT_NAME, "myservice geo from internet");
        }
        formatLocation(location);//Функция формирования геоданных
        new WriteFile().execute();//поток передачи данных на сервер
    }

    private void formatLocation(Location location){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        // new Date(location.getTime()
        String latitude = String.format("%1$.7f", location.getLatitude());
        latitude = latitude.replace(',', '.');
        String longitude = String.format("%1$.7f", location.getLongitude());
        longitude = longitude.replace(',', '.');
        Log.d(LOG_TAG, "set last geodata");
        Logs(ACCOUNT_NAME, "myservice set last geodata");
        last_lantitude = latitude;
        last_longitude = longitude;
        last_date = dateFormat.format(new Date(location.getTime()));
        last_time = timeFormat.format(new Date(location.getTime()));
    }

    //Асинхронный поток
    class WriteFile extends AsyncTask<String, String, String> {

        @Override
        //перед операцией
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        //операция
        protected String doInBackground(String... params) {
            Log.d(LOG_TAG, "GeoData to server");
            Logs(ACCOUNT_NAME, "myservice sended geodata to server");
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet("http://130.193.35.127/testtutor.php" + "?login="+login + "&latitude=" + last_lantitude+"&longitude="+last_longitude+"&date="+last_date+"&time="+last_time);
            try {
                HttpResponse response = httpclient.execute(httpget);
            }
            catch (ClientProtocolException e) {
            }
            catch (IOException e) {
            }
            return null;
        }

        @Override
        //после операции
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    public class MyThread extends Thread {
        private String login;
        private String log;
        public MyThread(String login, String log){
            this.login = login;
            Date dateNow = new Date();
            SimpleDateFormat formatForDateNow = new SimpleDateFormat("hh:mm:ss");
            this.log = (String.format(log + " " + formatForDateNow.format(dateNow))).replace(" ", "%20");
        }
        public void run() {
            HttpClient httpclient = new DefaultHttpClient();
            Log.d(LOG_TAG, "http://130.193.35.127/get_logs.php" + "?login=" + login + "&text=" + log);
            HttpGet httpget = new HttpGet("http://130.193.35.127/get_logs.php" + "?login=" + login + "&text=" + log);
            try {
                HttpResponse response = httpclient.execute(httpget);
            }
            catch (IOException e) {
                Log.d(LOG_TAG, e.toString());
            }
        }
    }

    public void Logs(String login, String log){
        MyThread t = new MyThread(login, log);
        t.start();
    }
}